package com.cx.ng.aml0001b.service;

import com.cx.ng.aml0001b.dto.AsiaMileParam;
import com.cx.ng.common.NgResponse;

/**
 * Created by cheng on 9/2/2016.
 */
public interface MilelageCalServiceInterface {
    NgResponse<Integer> calMilelage(AsiaMileParam param);
}
