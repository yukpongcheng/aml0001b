package com.cx.ng.aml0001b.service.impl;

import com.asiamiles.ixClsMileageCal.beans.calculator.*;
import com.cx.ng.aml0001b.dto.AsiaMileParam;
import com.cx.ng.aml0001b.service.MilelageCalServiceInterface;
import com.cx.ng.common.NgResponse;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestScoped
@Path("/MilelageCalService")
public class MilelageCalService implements MilelageCalServiceInterface
{
    @Inject
    MileageCalRedem mileageCalRedem;
    @POST
    @Path("/calMilelage")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public NgResponse<Integer> calMilelage(AsiaMileParam param)
    {
        MileageCalParam mcParam;
//        sectors.add(new MileageCalSector("HVB", "MBH", "QFA", null));
        List sectors = param.getSectorList().stream().map(s->new MileageCalSector(s.getOrigin(), s.getDestination(), s.getAirline(), null))
                .collect(Collectors.toList());
        mcParam = new MileageCalParam(1, param.getAwardType(), new ArrayList(sectors), "-1");

        MileageCalReturn mcReturn =  mileageCalRedem.redemMileage(mcParam);
        NgResponse<Integer> resp = new NgResponse<Integer>();
        if(mcReturn.getErrorCode() == null){
            int award = mcReturn.getMileage().stream().mapToInt(m->((MileageCalAward)m).getAm_Mileage()).sum();
            resp.setData(award);
        }else{
            resp.addErrorMessage(mcReturn.getErrorCode(0));
        }
        return resp;
    }

}