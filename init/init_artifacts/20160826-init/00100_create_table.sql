create table accrual_airport_pair
   (	fkx_o_airport_code varchar(4) not null ,
	fkx_d_airport_code varchar(4) not null ,
	marketing_mileage int(9),
	fkx1_carriercode varchar(3) not null ,
	flt_leg_type_code varchar(1),
	inactive_indicator varchar(1),
	modified_date datetime default current_timestamp not null ,
	 constraint pk_accrual_airport_pair primary key (fkx_o_airport_code, fkx_d_airport_code, fkx1_carriercode)

   );




   create table accrual_rate
   (	code varchar(1) not null ,
	descript varchar(25) not null ,
	percentage double(4,3) not null
   ) ;



  create table airport
   (	airport_code varchar(4) not null ,
	airport_name varchar(35),
	fkx_main_city_code varchar(6),
	last_updt_date datetime,
	last_updt_time datetime
   )  ;





  create table award_class
   (	award_code varchar(6) not null ,
	ticket_upgrade varchar(18) not null ,
	award_class varchar(32) not null
   )  ;


  create table award_exclude
   (	start_date datetime not null ,
	end_date datetime not null ,
	award varchar(12) not null
   ) ;



  create table award_type
   (	code varchar(6) not null ,
	short_desc varchar(62) not null ,
	fk2_rt_ow_ind_code varchar(4),
	fk1_mrch_clas_code int(2),
	fk3_redemp_cls_cd varchar(1)
   )  ;



  create table award_package
   (	fk1_carrier_code varchar(3) not null ,
	fk2_m_and_a_code varchar(6) not null ,
	mixed_carrier_flag varchar(1),
	fk1_package_code varchar(6) not null ,
	fk1_award_pgm_code varchar(1) not null ,
	award_value int(7) not null ,
	minimum_distance int(7) not null ,
	maximum_distance int(7) not null ,
	start_date datetime not null ,
	end_date datetime not null ,
	modified_date datetime default current_timestamp not null ,
	 constraint pk_award_package primary key (fk1_carrier_code, fk2_m_and_a_code, fk1_package_code, start_date, end_date)

   ) ;





  create table city
   (	iata_code varchar(4),
	location_name varchar(45) not null ,
	country_selected varchar(3)
   ) ;






  create table class_group
   (	start_date datetime not null ,
	end_date datetime not null ,
	fk2_mil_clas_code varchar(1) not null ,
	fk1_book_clas_code varchar(2) not null ,
	fk1_carrier_code varchar(3) not null
   )       ;

    create table carrier
   (	carrier_code varchar(3) not null ,
	carrier_number int(3),
	airline_designator varchar(2),
	carrier_name varchar(80) not null ,
	last_updt_date datetime,
	last_updt_time datetime,
	purge_date datetime,
	fk1_carrier_code varchar(3),
	disallow_open_jaw varchar(1) default 'n' not null ,
	disallow_stopover varchar(1) default 'n' not null ,
	disallow_transfer varchar(1) default 'n' not null
   ) ;


  create table carrier_item
   (	start_date datetime not null ,
	end_date datetime not null ,
	mixed_carrier_flag varchar(1),
	fk1_carrier_code varchar(3) not null ,
	fk2_m_and_a_code varchar(6) not null ,
	fk3_award_pgm_code varchar(4) not null ,
	fk3_award_pgm_type varchar(25) not null
   ) ;


  create table package_item
   (	fk2_package_code varchar(6) not null ,
	fk1_m_and_a_code varchar(6) not null
   ) ;

  create table program_package
   (	package_type varchar(15) not null ,
	award_value int(7),
	start_dt datetime not null ,
	end_dt datetime not null ,
	fk1_package_code varchar(6),
	fk2_pp_aaz_id varchar(3)
   )     ;


  create table program_zone
   (	identifier varchar(3) not null ,
	descript varchar(40) not null ,
	start_date datetime not null ,
	end_date datetime not null ,
	maximum_distance int(7) not null ,
	minimum_distance int(7) not null ,
	fk1_award_pgm_code varchar(4),
	fk1_award_pgm_type varchar(25)
   )  ;

  create table sector_accrual
   (	fkx_o_airport_code varchar(4) not null ,
	fkx_d_airport_code varchar(4) not null ,
	marketing_mileage int(9) not null ,
	last_updt_date datetime,
	last_updt_time datetime,
	fkx1_carriercode varchar(3) not null ,
	flt_leg_type_code varchar(1) not null ,
	flight_type_ind varchar(2)
   )   ;

  create table sector_redemption
   (	fkx_o_airport_code varchar(4) not null ,
	fkx_d_airport_code varchar(4) not null ,
	marketing_mileage int(9) not null ,
	last_updt_date datetime,
	last_updt_time datetime,
	fkx1_carriercode varchar(3) not null ,
	flt_leg_type_code varchar(1) not null ,
	flight_type_ind varchar(2)
   )    ;

  create table tutparm
   (	param_name varchar(16) not null ,
	param_type varchar(1) not null ,
	param_length int(2) not null ,
	decimal_places int(2),
	param_value varchar(40) not null ,
	descript varchar(250) not null ,
	user_id varchar(8) not null ,
	last_updt_time datetime not null ,
	last_updt_date datetime not null ,
	 constraint iutparm0 primary key (param_name)

   ) ;


  create table zone_exclude
   (	start_date datetime not null ,
	end_date datetime not null ,
	zone varchar(6) not null
   );
 
                                                                             