 create index idx_accrual_airport_pair_1 on accrual_airport_pair (fkx1_carriercode, fkx_o_airport_code, fkx_d_airport_code) ;
  create index i_airport_1 on airport (airport_code) ;
  create index idx_award_package_1 on award_package (fk2_m_and_a_code, fk1_carrier_code)   ;
  create index i_city_1 on city (iata_code) ;